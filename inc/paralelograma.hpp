#ifndef PARALELOGRAMA_HPP
#define PARALELOGRAMA_HPP

#include "formageometrica.hpp"

class Paralelograma : public FormaGeometrica{
private:
  float lado;
public:
  Paralelograma();
  Paralelograma(float base, float altura, float lado);
  ~Paralelograma();
  void set_lado(float lado);
  float get_lado();
  float calcula_area();
  float calcula_perimetro();
};

#endif
