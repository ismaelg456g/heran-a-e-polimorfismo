#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include "formageometrica.hpp"

class Pentagono : public FormaGeometrica{
private:

public:
  Pentagono();
  Pentagono(float base);
  ~Pentagono();
  float calcula_area();
  float calcula_perimetro();

};

#endif
