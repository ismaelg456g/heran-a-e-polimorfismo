#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP

#include "formageometrica.hpp"

class Triangulo: public FormaGeometrica{
private:
  float ladoA;
  float ladoB;
  float base;
public:
  Triangulo();
  Triangulo( float base, float altura, float ladoA, float ladoB);
  ~Triangulo();
  float calcula_area();
  float calcula_perimetro();
  void setLadoA(float ladoA);
  float getLadoA();
  void setLadoB(float ladoB);
  float getLadoB();
};

#endif
