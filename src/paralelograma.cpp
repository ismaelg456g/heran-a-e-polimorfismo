#include "paralelograma.hpp"

Paralelograma::Paralelograma(){
  set_tipo("Paralelograma");
  set_base(0.0f);
  set_altura(0.0f);
  set_lado(0.0f);
}
Paralelograma::Paralelograma(float base, float altura, float lado){
  set_tipo("Paralelograma");
  set_base(base);
  set_altura(altura);
  set_lado(lado);
}
Paralelograma::~Paralelograma(){}
void Paralelograma::set_lado(float lado){
  this->lado = lado;
}
float Paralelograma::get_lado(){
  return lado;
}
float Paralelograma::calcula_area(){
  return get_base()*get_altura();
}
float Paralelograma::calcula_perimetro(){
  return 2*get_base()+2*get_lado();
}
