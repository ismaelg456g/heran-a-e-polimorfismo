#include "triangulo.hpp"
#include <cmath>

Triangulo::Triangulo(){
  set_tipo("Triângulo");
  set_base(0.0f);
  set_altura(0.0f);
  setLadoA(0.0f);
  setLadoB(0.0f);
}
Triangulo::Triangulo( float base, float altura, float ladoA, float ladoB){
    set_tipo("Triângulo");
    set_base(base);
    set_altura(altura);
    setLadoA(ladoA);
    setLadoB(ladoB);
}
Triangulo::~Triangulo(){}
float Triangulo::calcula_area(){
  return get_base()*get_altura()/2;
}
float Triangulo::calcula_perimetro(){
  return get_base() + ladoA + ladoB;
}
void Triangulo::setLadoA(float ladoA){
  this->ladoA=ladoA;
}
float Triangulo::getLadoA(){
  return ladoA;
}
void Triangulo::setLadoB(float ladoB){
  this->ladoB=ladoB;
}
float Triangulo::getLadoB(){
  return ladoB;
}
