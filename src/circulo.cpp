#include "circulo.hpp"
#define PI 3.14159265359

Circulo::Circulo(){
  set_raio(0.0f);
  set_tipo("Círculo");
}
Circulo::Circulo(float raio){
  set_raio(raio);
  set_tipo("Círculo");
}
Circulo::~Circulo(){}
void Circulo::set_raio(float raio){
  this->raio = raio;
}
float Circulo::get_raio(){
  return raio;
}
float Circulo::calcula_area(){
  return PI*raio*raio;
}
float Circulo::calcula_perimetro(){
  return 2*PI*raio;
}
