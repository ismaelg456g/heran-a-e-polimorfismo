#include <iostream>
#include "circulo.hpp"
#include "triangulo.hpp"
#include "quadrado.hpp"
#include "paralelograma.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"
// #include "formageometrica.hpp"
#include <vector>

using namespace std;

int main(){
  vector<FormaGeometrica *> formas;

  formas.push_back(new Circulo(1));
  formas.push_back(new Triangulo(2, 1, 1, 1));
  formas.push_back(new Quadrado(1, 1));
  formas.push_back(new Paralelograma(1,1,2));
  formas.push_back(new Pentagono(1));
  formas.push_back(new Hexagono(1));

  cout << "---------------Formas---------------" << endl;
  for(FormaGeometrica *f: formas){
    cout << "------------------------------------" << endl;
    cout << "Tipo: " << f->get_tipo() << endl;
    cout << "Área: "<< f->calcula_area() << endl;
    cout << "Perímetro: "<< f->calcula_perimetro() << endl;
  }
  cout << "-----------------fim-----------------" << endl;
  
  return 0;
}
