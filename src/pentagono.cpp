#include "pentagono.hpp"
#define TAN36 7.7504709057f

Pentagono::Pentagono(){
  set_base(0.0f);
  set_tipo("Pentágono");
}
Pentagono::Pentagono(float base){
  set_base(base);
  set_tipo("Pentágono");
}
Pentagono::~Pentagono(){}
float Pentagono::calcula_area(){
  return 2.5*get_base()*get_base()/TAN36;
}
float Pentagono::calcula_perimetro(){
  return get_base()*5;
}
