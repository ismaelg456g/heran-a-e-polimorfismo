#include "quadrado.hpp"

Quadrado::Quadrado(){
  set_tipo("Quadrado");
  set_base(0.0f);
  set_altura(0.0f);
}
Quadrado::Quadrado(float base, float altura){
  set_tipo("Quadrado");
  set_base(base);
  set_altura(altura);
}
Quadrado::~Quadrado(){}
