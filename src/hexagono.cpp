#include "hexagono.hpp"
#define RAIZ3 1.73205080757


Hexagono::Hexagono(){
  set_base(0.0f);
  set_tipo("Hexágono");
}
Hexagono::Hexagono(float base){
  set_base(base);
  set_tipo("Hexágono");
}
Hexagono::~Hexagono(){}
float Hexagono::calcula_area(){
  return 1.5*RAIZ3*get_base()*get_base();
}
float Hexagono::calcula_perimetro(){
  return 6*get_base();
}
